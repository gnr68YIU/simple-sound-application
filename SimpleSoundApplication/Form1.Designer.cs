﻿namespace SimpleSoundApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dualStatebutton = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dualStateButton2 = new System.Windows.Forms.CheckBox();
            this.dualStateButton3 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dualSenseButton4 = new System.Windows.Forms.CheckBox();
            this.dualSenseButton5 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dualStatebutton
            // 
            this.dualStatebutton.Appearance = System.Windows.Forms.Appearance.Button;
            this.dualStatebutton.AutoSize = true;
            this.dualStatebutton.Location = new System.Drawing.Point(22, 50);
            this.dualStatebutton.Name = "dualStatebutton";
            this.dualStatebutton.Size = new System.Drawing.Size(201, 34);
            this.dualStatebutton.TabIndex = 0;
            this.dualStatebutton.Text = "Back To The Future";
            this.dualStatebutton.UseVisualStyleBackColor = true;
            this.dualStatebutton.CheckedChanged += new System.EventHandler(this.dualStatebutton_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dualStateButton3);
            this.groupBox1.Controls.Add(this.dualStateButton2);
            this.groupBox1.Controls.Add(this.dualStatebutton);
            this.groupBox1.Font = new System.Drawing.Font("Calisto MT", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(41, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(386, 139);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Classic Movies";
            // 
            // dualStateButton2
            // 
            this.dualStateButton2.Appearance = System.Windows.Forms.Appearance.Button;
            this.dualStateButton2.AutoSize = true;
            this.dualStateButton2.Location = new System.Drawing.Point(253, 50);
            this.dualStateButton2.Name = "dualStateButton2";
            this.dualStateButton2.Size = new System.Drawing.Size(116, 34);
            this.dualStateButton2.TabIndex = 1;
            this.dualStateButton2.Text = "The Beach";
            this.dualStateButton2.UseVisualStyleBackColor = true;
            this.dualStateButton2.CheckedChanged += new System.EventHandler(this.dualStateButton2_CheckedChanged);
            // 
            // dualStateButton3
            // 
            this.dualStateButton3.Appearance = System.Windows.Forms.Appearance.Button;
            this.dualStateButton3.AutoSize = true;
            this.dualStateButton3.Location = new System.Drawing.Point(22, 99);
            this.dualStateButton3.Name = "dualStateButton3";
            this.dualStateButton3.Size = new System.Drawing.Size(251, 34);
            this.dualStateButton3.TabIndex = 2;
            this.dualStateButton3.Text = "The Fast and the Furious";
            this.dualStateButton3.UseVisualStyleBackColor = true;
            this.dualStateButton3.CheckedChanged += new System.EventHandler(this.dualStateButton3_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dualSenseButton5);
            this.groupBox2.Controls.Add(this.dualSenseButton4);
            this.groupBox2.Font = new System.Drawing.Font("Calisto MT", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(469, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(446, 136);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Classic Games";
            // 
            // dualSenseButton4
            // 
            this.dualSenseButton4.Appearance = System.Windows.Forms.Appearance.Button;
            this.dualSenseButton4.AutoSize = true;
            this.dualSenseButton4.Location = new System.Drawing.Point(6, 31);
            this.dualSenseButton4.Name = "dualSenseButton4";
            this.dualSenseButton4.Size = new System.Drawing.Size(79, 34);
            this.dualSenseButton4.TabIndex = 0;
            this.dualSenseButton4.Text = "Doom";
            this.dualSenseButton4.UseVisualStyleBackColor = true;
            this.dualSenseButton4.CheckedChanged += new System.EventHandler(this.dualSenseButton4_CheckedChanged);
            // 
            // dualSenseButton5
            // 
            this.dualSenseButton5.Appearance = System.Windows.Forms.Appearance.Button;
            this.dualSenseButton5.AutoSize = true;
            this.dualSenseButton5.Location = new System.Drawing.Point(138, 31);
            this.dualSenseButton5.Name = "dualSenseButton5";
            this.dualSenseButton5.Size = new System.Drawing.Size(102, 34);
            this.dualSenseButton5.TabIndex = 1;
            this.dualSenseButton5.Text = "Wipeout";
            this.dualSenseButton5.UseVisualStyleBackColor = true;
            this.dualSenseButton5.CheckedChanged += new System.EventHandler(this.dualSenseButton5_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 315);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Basic Sound Application";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox dualStatebutton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox dualStateButton2;
        private System.Windows.Forms.CheckBox dualStateButton3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox dualSenseButton5;
        private System.Windows.Forms.CheckBox dualSenseButton4;
    }
}

